import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import scipy.ndimage as ndimage
import scipy.misc as misc
from glob import glob
import random
import time

__version__ = '1.1'

# Global Variables
res = 255
mid = (res-1)/2 					# midpoint of image
image = np.zeros((res,res))			# square image of resolution res
image_list = []
num_im = 100 							# number of images
fps = 30							# frames per second
mspf = int(1000/fps)				# milliseconds per frame
bin_imgs = []
#grad_imgs = []
harris_imgs = []
#grad_lbl = []
#grad_feats = []
harris_lbl = []
harris_feats = []
MM = []
IX_gw = []
IY_gw = []
vtx_cntrs = []

def IsInsidePolygon(point,vertex_list):
	for i in range(len(vertex_list)):			# For each vertex, create vectors b/w
		a = point - vertex_list[i-1]			#	vertex & point,
		b = vertex_list[i] - vertex_list[i-1]	#	and vertex and previous vertex.
		if np.cross(a,b) < 0:					# Do cross product, and since rotating
			return 0 							#	clockwise, if ever neg then point
	return 1 									#	is not inside the polygon

def GenerateVertices():
	global res, mid
	center = np.array([mid,mid])
	while True:		
		Ax = mid 							# Set A on y Axis
		Ay = random.randint(mid+1,res-1)
		A = np.array([Ax,Ay])
		if np.linalg.norm(A-center) < mid:	# Ensure distance from center small enough
			print "Vertex A: ",A 			# 	so that will not leave edge upon rot.
			break
	while True:
		Bx = random.randint(mid-1,res-1)	# Set B in 4th quadrant
		By = random.randint(0,mid)
		B = np.array([Bx,By])
		if np.linalg.norm(B-center) < mid:	# see above
			print "Vertex B: ",B			
			break
	while True:
		Cx = random.randint(0,mid)			# Set C in 3rd quadrant
		Cy = random.randint(0,mid)
		C = np.array([Cx,Cy])
		if np.linalg.norm(C-center) < mid:	# see above
			print "Vertex C: ",C
			break
	return [A,B,C]

def GenerateRandomTriangle(sigma = 0):
	global image
	vertex_list = GenerateVertices()		# Generate random vertices
	for xx in range(res):					# Iterate through image, evaluate
		for yy in range(res):				#	each point to see if inside
			point = np.array([xx,yy])
			image[yy,xx] = IsInsidePolygon(point,vertex_list) 
	image = ndimage.gaussian_filter(image,sigma)

def SpinAndSave(image,rate,nimages=100,prefix="triangle"):
	global image_list
	image_list.append(image)
	for ii in range(nimages):				# Image rotation 
		angle = float(rate)/float(fps)*ii 
		rotated_image = ndimage.rotate(image, angle, reshape=False)
		image_list.append(rotated_image)

	print "Saving files . . . .",
	for ii in range(len(image_list)):		# Save frames to file
		file_name = '%s_%03d.png' % (prefix, ii)
		misc.imsave(file_name, image_list[ii])
	print ". . . . Complete"

def Animate(images = image_list, save = False, name='triangle'):
	fig = plt.figure()
	ims = []
	for ii in range(len(images)):			# Build array of image plots, then animate
		im = plt.imshow(images[ii], origin='lower', cmap=plt.cm.gray)
		ims.append([im])
	ani = animation.ArtistAnimation(fig, ims, interval=mspf, blit=True, repeat=False)
	plt.axis('off')

	if save == True:				# If saving movie to file, do not show on screen
		print "Saving movie . . . .",
		file_name = 'rotating_%s.mp4' % (name)		
		ani.save(file_name)
		print ". . . . Complete"
	else:
		plt.show()

def getBinaryImage(imgs):
	global res, num_im, mid
	bin_list = []
	bin_struct = np.ones((5,5))
	for i in range(0, num_im):
		b_c = np.zeros((res,res))
		c_img = imgs[i]
		b_c = ndimage.binary_closing(c_img,structure=bin_struct).astype(np.int)
		b_c = ndimage.binary_opening(c_img,structure=bin_struct).astype(np.int)
		b_c = c_img-b_c
		bin_list.append(b_c)
	for ii in range(0,len(bin_list)):		# Save frames to file
		file_name = '%s_%03d.png' % ("bn", ii)
		misc.imsave(file_name, bin_list[ii])
	return bin_list

# 1st vertex detection method - analyze gradient magnitude of images
# (misunderstood the problem statement)

#def getGradientImage(imgs):
#	global res, num_im
#	sigma1 = 3
#	grad_list = []
#	for i in range (0, num_im):
#		c_img = imgs[i]
#		grad1_c = ndimage.gaussian_gradient_magnitude(c_img,sigma1,mode='constant')
#		grad1_c *= 1/np.max(grad1_c)		#normailze back to 1
#		th_c = ndimage.white_tophat(grad1_c,size=(3,3),mode='constant') 		#lessens vertices, sharpens edges
#		th_med_c = ndimage.median_filter(th_c,size=3,mode='constant') 		#sharpens previous image
#		th_med_c *= 1/np.max(th_med_c)
#		grad2_c = ndimage.gaussian_gradient_magnitude(c_img,sigma1/2,mode='constant')
#		diff_c = grad2_c - th_med_c 	#removes edges from slightly blurred perimeter
#		diff_c *= 1/np.max(diff_c)
#		pct_c = diff_c			#loop prep
#		for j in range(0,2):	#intensify vertices
#			pct_c = ndimage.percentile_filter(pct_c,95,5,mode='constant')
#			pct_c *= 1/np.max(pct_c)
#		vert_c = pct_c > .85				#isolate vertices
#		vert_c = vert_c.astype(np.int)
#		grad_list.append(vert_c)
#	for ii in range(0,len(grad_list)):		# Save frames to file
#		file_name = '%s_%03d.png' % ("grad", ii)
#		misc.imsave(file_name, grad_list[ii])
#	return grad_list

def getHarrisImage(imgs,kappa=0.05,windowSize = 9,windowSpace = 2, sigma = 2):		# Vertex indentification via Harris filter
	global res, num_im, MM
	IX = np.zeros((res,res))
	IY = np.zeros((res,res))
	wsz = windowSize
	wctr = (wsz-1)/2
	gw_list = []
	H_list = []
	gwts = np.zeros((wsz,wsz))
	gwts[wctr,wctr] = 1 
	gwts = ndimage.gaussian_filter(gwts, sigma)
	print "Applying Harris Filter..."
	for i in range(0,num_im):
		c_img = imgs[i]
		vert_c = np.zeros((res,res))
		c_M = np.zeros((2,2))
		M_list = []
		MC = np.zeros((res,res))
		IX = ndimage.sobel(c_img,axis=0,mode='constant')	# directional sobel filters per image
		IY = ndimage.sobel(c_img,axis=1,mode='constant')
		IX_gw_c = np.zeros((res,res))		# for L-K method
		IY_gw_c = np.zeros((res,res))
		IX2 = np.multiply(IX,IX)
		IY2 = np.multiply(IY,IY)
		IXIY = np.multiply(IX,IY)
		for y2 in range(wctr,res-wctr,windowSpace):	# Create gaussian weights for M
			M_list_temp = []
			for x2 in range(wctr,res-wctr,windowSpace):
				xtemp1 = x2-wctr
				xtemp2 = x2+wctr+1
				ytemp1 = y2-wctr
				ytemp2 = y2+wctr+1
				IX_gw_c[x2,y2] = np.sum(np.multiply(IX[xtemp1:xtemp2,ytemp1:ytemp2],gwts))
				IY_gw_c[x2,y2] = np.sum(np.multiply(IY[xtemp1:xtemp2,ytemp1:ytemp2],gwts))
				c_M[0,0]= np.sum(np.multiply(IX2[xtemp1:xtemp2,ytemp1:ytemp2],gwts))
				c_M[1,1]= np.sum(np.multiply(IY2[xtemp1:xtemp2,ytemp1:ytemp2],gwts))
				c_M[1,0] = c_M[0,1] = np.sum(\
					np.multiply(IXIY[xtemp1:xtemp2,ytemp1:ytemp2],gwts))
				M_list_temp.append(c_M)			# for lucas-kanade; list of 2x2's
				detM = np.linalg.det(c_M)
				MC[x2,y2] = detM - kappa*(np.trace(c_M)**2)
			M_list.append(M_list_temp)			# for lucas-kanade; list of lists of 2x2's
		MC *= 1/np.max(MC)
		MC = MC > 0.05
		MC = MC.astype(np.int)
		MC = ndimage.maximum_filter(MC,size=(5,5),mode='constant')
		MC = ndimage.binary_erosion(MC,iterations = 1)
		IX_gw.append(IX_gw_c)
		IY_gw.append(IY_gw_c)
		H_list.append(MC)
		MM.append(M_list)	# sequentinal list of spatial lists of spatial lists of 2x2's
		# Output to verify that program is running
		if (i+1)%10 == 0:
			print "."
		else:
			print".",
	for ii in range(0,len(H_list)):		# Save frames to file
		file_name = '%s_%03d.png' % ("H", ii)
		misc.imsave(file_name, H_list[ii])
	return H_list

def locateVertices(vtcs):
	lbl = []
	feats = []
	for i in range(0,len(vtcs)):
		lbl_c, feats_c = ndimage.label(vtcs[i])			#review syntax
		lbl.append(lbl_c)
		feats.append(feats_c)
		if feats_c != 3:
			print feats_c
	return (lbl,feats)

def getLKFlowFields(lbl, feats):		# Incomplete, currently does not work due to zero-valued M's
	global res, num_im, MM, IX_gw, IY_gw, fps
	MM_inv = []
	vx = []
	vy = []
	dt = 1/fps
	for i in range(0,num_im):
		M_inv = []
		vx_c = np.zeros((res,res))
		vy_c = np.zeros((res,res))
		for j in range(0,np.max(feats)):			# create inverses of each M
			M_inv_temp = []
			xbnd1 = int(np.floor(vtx_cntrs[j][i][1]-5))
			xbnd2 = int(np.ceil(xbnd1+11))				# round for non-integer cases
			ybnd1 = int(np.floor(vtx_cntrs[j][i][0]-5))
			ybnd2 = int(np.ceil(ybnd1+11))
			#if xbnd1<0:	xbnd1=0 	#necessity unknown
			#if xbnd2<0:	xbnd2=0
			#if ybnd1<0:	ybnd1=0
			#if ybnd2<0:	ybnd2=0
			for y3 in range(ybnd1,ybnd2):
				for x3 in range(xbnd1,xbnd2):
					print i, y3, x3
					print MM[i][y3][x3]						# center pizel can have M=0??
					M_inv_c = np.linalg.inv(MM[i][y3][x3])		# inverts M[i] at x3,y3
					if i==num_im-1:
						bb = np.array([0],[0])		# no future time data
		#			else:
		#				IXc = IX_gw[i]		#current gw masks
		#				IYc = IY_gw[i]
						#bb = np.array([-1*IXc(x3,y3)*1],[-1*IYc(x3,y3)*1])			#-[<IX>*It; <IY>*It]
					#(vx_c, vy_c) = np.dot(M_inv_c,bb)
					M_inv_temp.append(M_inv_c)
				M_inv.append(M_inv_temp)
			MM_inv.append(M_inv)
		vx.append(vx_c)
		vy.append(vy_c)

def getAvgPixel(lbl, feats):			# used to locate regions for velocity field solving
	global vtx_cntrs
	global MM
	for j in range(1,4):
		cntrs_tmp = []
		for i in range(0,len(lbl)):
			# may need to swap av_x and av_y
			(av_y, av_x) = ndimage.center_of_mass(lbl[i],labels=lbl[i],index=j)	# estimate center of vertex
			cntrs_tmp.append((av_y,av_x))
			print av_x, av_y #, MM[i][int(np.round(av_y))][int(np.round(av_x))]
		vtx_cntrs.append(cntrs_tmp) # m x n x 2: m features, n images

def getAvgPixel2(lbl,imgs):
	global vtx_cntrs
	ws = 5
	[Av_y,Av_x] = ndimage.center_of_mass(lbl[0],labels=lbl[0],index=1)
	[Bv_y,Bv_x] = ndimage.center_of_mass(lbl[0],labels=lbl[0],index=2)
	[Cv_y,Cv_x] = ndimage.center_of_mass(lbl[0],labels=lbl[0],index=3)
	cntrs_tmp = [[Av_x,Av_y],[Bv_x,Bv_y],[Cv_x,Cv_y]] 
	vtx_cntrs.append(cntrs_tmp)
	for ii in range(len(cntrs_tmp)):
		for jj in range(2):
			cntrs_tmp[ii][jj] = round(cntrs_tmp[ii][jj])
	for ii in range(1,len(imgs)):
		ay_lb = cntrs_tmp[0][1]-ws; ay_ub = cntrs_tmp[0][1]+ws
		ax_lb = cntrs_tmp[0][0]-ws; ax_ub = cntrs_tmp[0][0]+ws
		by_lb = cntrs_tmp[1][1]-ws; by_ub = cntrs_tmp[1][1]+ws
		bx_lb = cntrs_tmp[1][0]-ws; bx_ub = cntrs_tmp[1][0]+ws
		cy_lb = cntrs_tmp[2][1]-ws; cy_ub = cntrs_tmp[2][1]+ws
		cx_lb = cntrs_tmp[2][0]-ws; cx_ub = cntrs_tmp[2][0]+ws
		[Av_y,Av_x] = ndimage.center_of_mass(imgs[ii][ay_lb:ay_ub,ax_lb:ax_ub])
		[Bv_y,Bv_x] = ndimage.center_of_mass(imgs[ii][by_lb:by_ub,bx_lb:bx_ub])
		[Cv_y,Cv_x] = ndimage.center_of_mass(imgs[ii][cy_lb:cy_ub,cx_lb:cx_ub])
		cntrs_tmp = [[Av_x+ax_lb,Av_y+ay_lb],[Bv_x+bx_lb,Bv_y+by_lb],[Cv_x+cx_lb,Cv_y+cy_lb]] 
		# print "Centers: ",cntrs_tmp
		vtx_cntrs.append(cntrs_tmp)
		for ii in range(len(cntrs_tmp)):
			for jj in range(2):
				cntrs_tmp[ii][jj] = round(cntrs_tmp[ii][jj])

def plotVertexPaths():
	#plt.
	pass

GenerateRandomTriangle(2)
SpinAndSave(image,rate = 40,nimages = num_im)
Animate(save=True)

# image manipulation

#bin_imgs = getBinaryImage(image_list)
#grad_imgs = getGradientImage(image_list)
harris_imgs = getHarrisImage(image_list,0.05,windowSize = 9,windowSpace = 3)
#(grad_lbl, grad_feats) = locateVertices(grad_imgs)
(harris_lbl, harris_feats) = locateVertices(harris_imgs)
#getLKFlowFields(grad_lbl)
getAvgPixel2(harris_lbl, harris_imgs)
#getLKFlowFields(harris_lbl, harris_feats)

Animate(images = harris_imgs, save=True, name='vertices')
